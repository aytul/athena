/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/
#ifndef AthenaMonitoring_Monitored_h
#define AthenaMonitoring_Monitored_h

/**
 * @file  AthenaMonitoring/Monitored.h
 * @brief Header file to be included by clients of the Monitored infrastructure
 */
#include "AthenaMonitoring/MonitoredGroup.h"
#include "AthenaMonitoring/MonitoredScalar.h"
#include "AthenaMonitoring/MonitoredCollection.h"
#include "AthenaMonitoring/MonitoredTimer.h"

#endif
