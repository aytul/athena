/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigEFLongLivedParticles_page 
@author Antonio Policicchio

@section TrigEFLongLivedParticles_TrigEFLongLivedParticlesOverview Overview
This package contains EF Fex algorithm for Long-Lived 
neutral particle triggers: TrigLoFRemoval.
The TrigLoFRemoval is accessing the EF jet collection and the cal cell container.
It saves the cells associated in PHI with the leading jets for LoF removal in the HYPO
algorithm TrigLongLivedParticlesHypo/LoFRemovalHypo.h 



*/
